<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostComment extends Model
{
    use SoftDeletes;

    protected $table = 'posts_comments';

    protected $fillable = [
        'id',
        'username',
        'post_id',
        'body',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
