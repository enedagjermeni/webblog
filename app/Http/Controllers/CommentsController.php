<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use App\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $comments = PostComment ::all();

        return view('posts.index', compact('comments'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $user = Auth::user();
            $commentByUser = PostComment::create([
                'post_id' => $request->post,
                'username' =>$user->name,
                'body' => $request->body
            ]);

            return  redirect()->back()->with('message', 'Comment saved successfully!');
        }
        catch (\Exception $e) {
            return  redirect()->back()->with('message', $e->getMessage());
        }
    }
}
