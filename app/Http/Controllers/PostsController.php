<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use App\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $posts = Post ::all();

        return view('posts.index', compact('posts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'title' => 'required',
                'cover_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name =  url('/images').'/'.time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $name);
            }
            else {
                $name = 'noimage.png';
            }
            $request->request->add(['cover_image' => $name]);

            $posts = Post::create($request->all());

            return  redirect()->back()->with('message', 'Post saved successfully!');
        }
        catch (\Exception $e) {
            return  redirect()->back()->with('message', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $postComments = DB::table('posts_comments')
            ->select ('*')
            ->where('post_id', $post->id)
            ->get();

        return view('posts.show', compact('post','postComments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::find($id);

        return view('posts.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        try {
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = url('/images') . '/' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $name);
            } else {
                $name = $post->cover_image;
            }
            $request->request->add(['cover_image' => $name]);

            $post->update($request->all());

            return  redirect()->back()->with('message', 'Post updated successfully!');

        }
        catch (\Exception $e) {
            return  redirect()->back()->with('message', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $posts = Post::find($id);
            $posts->delete();

            return  redirect()->back()->with('message', 'Post deleted successfully!');
        }
        catch (\Exception $e) {
            return  redirect()->back()->with('message', $e->getMessage());
        }
    }
}
