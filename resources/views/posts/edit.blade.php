@extends('base')

@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-6">Edit post</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                    <form action="{{ route("posts.update", [$posts->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                    @if (session()->has('message'))
                        <div class="alert alert-info">
                            {{ session('message') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="title">Post Title*</label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($posts) ? $posts->title : '') }}" required>

                    </div>
                    <div class="form-group">
                        <label for="description">Description</label><br>
                        <textarea class="form-control" name="description" rows="5" cols="20">{{ old('description', isset($posts) ? $posts->description : '') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="cover_img">Cover Image</label><br>
                        <img src="{{ $posts->cover_image }}"  width="200" height="150" class="rounded">
                        <input type="file" id="image" name="image" class="form-control" value="{{ old('cover_image', isset($posts) ? $posts->cover_image : '') }}">
                    </div>
                    <div>
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div><br>
                    <a class="btn btn-xs btn-info" href="{{ route('posts.index') }}">
                        Go Back
                    </a>
                </form>
            </div>
        </div>
    </div>
@endsection