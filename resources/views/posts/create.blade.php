@extends('base')

@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-6">Add a post</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                    <form action="{{ route("posts.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if (session()->has('message'))
                            <div class="alert alert-info">
                                {{ session('message') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="title">Post Title*</label>
                            <input type="text" id="title" name="title" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" rows="5" cols="20"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cover_img">Cover Image</label>
                            <input type="file" id="image" name="image" class="form-control">
                        </div>
                        <div>
                            <input class="btn btn-primary" type="submit" value="Save">
                        </div><br>
                        <a class="btn btn-xs btn-info" href="{{ route('posts.index') }}">
                            Go Back
                        </a>
                    </form>
            </div>
        </div>
    </div>
@endsection