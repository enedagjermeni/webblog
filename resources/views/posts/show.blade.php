@extends('base')

@section('main')
    <style type="text/css">
        .post {
            color: #464646;
            font-family: 'proxima-nova', Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.5em;
            background: #f0f0f0;
            padding: .75rem 1.25rem;
        }
        .post-title{
            margin: 3px 0 10px 0;
            min-height: 0px;
            font-weight: 700;
            font-size: 28px;
            line-height: 36px;
        }

        .singlepostinfo{
            font-size: 15px;
            color: #999;
            margin: 0 0 15px;
        }
        .post-content p{
            font-size: 15px;
            line-height: 1.8em;
        }
        .comment{
            background:#fff !important;
            color:#999;
            width: 500px;
            height: 100px;
            font-family: 'proxima-nova', Arial,Tahoma,Verdana;
            font-size:15px;
            padding: 10px 10px;
            margin:5px 0 5px 10px;
            border:1px solid #dfdfdf;
            outline: none;
        }
        .comment-header{
            font-family: "Proxima Nova" , 'proxima-nova', Arial, Helvetica, sans-serif;
            font-size: 17px;
            display: inline-block;
            margin: 20px 0 0;
            margin-left: 10px;
            color: #464646;
        }
    </style>
    <div class="card">
        <h2 class="card-header">
            Post Details
        </h2>

        <div class="post">

            <div class="post-header">
                <h1 class="post-title"> {{ $post->title }}</h1>
            </div>
            <div class="singlepostinfo">
                Last updated on {{ $post->updated_at }} </div>
            <div class="post-content">
                <p>
                    <img src="{{ $post->cover_image}}" width="300" class="rounded" >
                </p>
                <p>
                    {!! $post->description !!}
                </p>
            </div>
        </div><br>
        <a class="btn btn-xs btn-warning" style="margin-bottom: 10px" href="#">
            Leave a reply
        </a>
        <div class="row">
            <div class="col-sm-4 offset-sm-2" style="margin-left: 10px">

                    <form action="{{ route("comments.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if (session()->has('message'))
                            <div class="alert alert-info">
                                {{ session('message') }}
                            </div>
                        @endif
                        <input type="hidden"  name="post" value= "{{ $post->id }}">

                        <div class="form-group">
                            <textarea class="form-control" name="body" rows="5" cols="20" placeholder="Your comment" required></textarea>
                        </div>
                        <div>
                            <input class="btn btn-primary" type="submit" value="Submit comment">
                        </div>
                    </form>
                </div>
            </div>

            @foreach($postComments as $key => $comments)
                <div class="comment-header">
                    <b>{{ $comments->username ?? '' }} </b>
                    <br>
                     {{ $comments->created_at ?? '' }}
                </div>
                <div class = "comment">
                     {{ $comments->body ?? '' }}
                </div>
            @endforeach
        </div>
    </div>

@endsection
