@extends('base')

@section('main')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("posts.create") }}">
                Add Post
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-6">Posts</h1>
            <table class=" table table-bordered table-striped table-hover datatable" id="example">
                <thead>
                <tr>
                    <th width="10">
                        Nr
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Cover image
                    </th>

                    <th>
                        Actions   &nbsp;
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($posts as $key => $post)
                    <?php $i++ ?>
                    <tr data-entry-id="{{ $post->id }}">
                        <td>
                            {{$i}}
                        </td>
                        <td>
                            {{ $post->title ?? '' }}
                        </td>
                        <td>
                            {{ $post->description ?? '' }}
                        </td>
                        <td>
                            <img src="{{ $post->cover_image}}"  style="width:90%" align="center" class="rounded" >
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('posts.show', $post->id) }}">
                               View
                            </a>

                            <a class="btn btn-xs btn-warning" href="{{ route('posts.edit', $post->id) }}">
                                Edit
                            </a>

                            <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
            </div>
@endsection