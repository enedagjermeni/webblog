<?php
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        \DB::table('users')->delete();
        \App\User::create(array(
            'name'     => 'Admin',
            'email'    => 'admin@administrator.com',
            'password' => '$2y$10$R6k0BUQUESWNlSXIvkaR2uKKraBqjs6KdnBM4m.25NH3Bg28Wp6cO',
        ));
    }

}