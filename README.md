-- Requirements

PHP >= 7.4

-- Steps to setup project

create .env file

composer install

composer require laravel/ui

php artisan key:generate

php artisan migrate

php artisan db:seed // the credentials are: admin@administrator.com / 1234567

php artisan serve